package ru.tsc.chertkova.tm;

import static ru.tsc.chertkova.tm.constant.TerminalConst.*;

public final class Application {

    public static void main(final String[] args) {
        displayWelcome();
        run(args);
    }

    private static void displayHelp() {
        System.out.println("[HELP]");
        System.out.println("version - Display program version.");
        System.out.println("about - Display developer info.");
        System.out.println("help - Display list of terminal commands.");
        System.exit(0);
    }

    private static void displayVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.0.0");
        System.exit(0);
    }

    private static void displayAbout() {
        System.out.println("[ABOUT]");
        System.out.println("Yuliya Chertkova");
        System.out.println("ychertkova@t1-consulting.com");
        System.exit(0);
    }

    private static void displayWelcome() {
        System.out.println("** WELCOME TO TASK MANAGER **");
    }

    private static void run(final String[] args) {
        if (args == null || args.length < 1) return;
        final String param = args[0];
        if (param == null || param.isEmpty()) return;
        switch (param) {
            case CMD_HELP:
                displayHelp();
                break;
            case CMD_VERSION:
                displayVersion();
                break;
            case CMD_ABOUT:
                displayAbout();
                break;
            default:
                showError(param);
        }
    }

    private static void showError(final String param) {
        System.out.printf("Error! This argument '%s' not supported...\n", param);
    }

}
